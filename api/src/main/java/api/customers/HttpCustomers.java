package api.customers;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

import java.util.Optional;
import java.util.UUID;

@Client("customers")
public interface HttpCustomers {

    @Get("/")
    Iterable<Customer>list();

    @Post("/")
    Customer post(@Body Customer customer);

    @Get("/{id}")
    Optional<Customer> get(UUID id);

}
