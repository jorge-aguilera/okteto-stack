package api;

import api.customers.Customer;
import api.customers.HttpCustomers;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import java.util.Optional;
import java.util.UUID;

@Controller("/api")
public class ApiController {

    HttpCustomers httpCustomers;

    public ApiController(HttpCustomers httpCustomers) {
        this.httpCustomers = httpCustomers;
    }

    @Get("/")
    Iterable<Customer>list(){
        return httpCustomers.list();
    }

    @Post("/")
    Customer post(Customer customer){
        return httpCustomers.post(customer);
    }

    @Get("/{id}")
    Optional<Customer> get(UUID id){
        return httpCustomers.get(id);
    }

}
