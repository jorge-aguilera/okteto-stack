package customers;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.reactivex.Flowable;

import java.util.Optional;
import java.util.UUID;

@Controller("/")
public class CustomerController {

    CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Get("/")
    public Flowable<CustomerEntity>list(){
        return Flowable.fromIterable( customerRepository.findAll() );
    }

    @Post("/")
    public CustomerEntity post(CustomerEntity customerEntity){
        return customerRepository.save(customerEntity);
    }

    @Get("/{id}")
    public Optional<CustomerEntity>get(UUID id){
        return customerRepository.findById(id);
    }

}
