# Okteto Stack

Ejemplo de dos microservicios desplegado mediante okteto stack

## API

front end que redirige llamadas a customer

## Customer

servicio con persistencia en base de datos

## Desplegar servicios

Una vez que se tiene un cluster en Okteto desplegar los servicios:

- cd customers
- okteto stack deploy
- cd ..
- cd api
- okteto stack deploy


